package com.game.view;

import com.game.controller.Controller;
import com.game.model.Johny;
import com.game.model.Player;

/**
 * Class for output text information.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public final class TextInfo {
    /**
     * @value controller - Controller object
     */
    private static Controller controller = new Controller();

    /**
     * @see TextInfo#TextInfo()
     */
    private TextInfo() {
    }

    /**
     * @see TextInfo#instruction()
     */
    static void instruction() {
        System.out.println("\nInstruction:\n\tThe game starts with "
                + "an random selection of shooter.\n\t"
                + "If you are first one, you will see a "
                + "massage \"You starts!\"\n\t"
                + "When you will be ready to start shooting at "
                + "your opponent\n\t"
                + "press \"Space+Enter\".Otherwise, if your "
                + "opponent is first,\n\t"
                + "you will see a massage \"Johny starts!\". "
                + "You can protect\n\t"
                + "yourself by clicking on \")+Enter\" buttons,"
                + " but you have\n\t"
                + "for this action only 2 seconds at first time "
                + "(time will\n\t"
                + "decrease). You will shoot one by one.After 6th shoot you "
                + "\n\twill see your and your opponent score."
                + "The game will \n\tcontinue after 3th second. "
                + "Looser will have"
                + "0 XP.\n\t\t\t\t\t\t\t\t\t\t\t\tGood luck!\n");
    }

    /**
     * @see TextInfo#welcoming()
     */
    static void welcoming() {
        Shoot.leftShootDesign();
        System.out.print("SHOOTMASTER");
        Shoot.rightShootDesign();
        TextInfo.instruction();
        Shoot.leftShootDesign();
        System.out.print(". . . . . ");
        Shoot.rightShootDesign();
    }

    /**
     * @see TextInfo#beginning()
     */
    static void beginning() {
        System.out.println("\nPress \"1+Enter\" if you are ready to start!");
        Input.input("1\n");
        System.out.println("\t\t\t\t\t\t" + controller.shooterSelection()
                + " starts!");
    }

    /**
     * @see TextInfo#winning()
     */
    public static void winning() {
        System.out.println("\t\t\t\t\t\tCongratulations!");
        System.out.println("\t\t\t\t\t\tYou won with " + Player.getXp()
                + "XP.");
    }

    /**
     * @see TextInfo#losing()
     */
    public static void losing() {
        System.out.println("\t\t\t\t\t\t\tSorry!");
        System.out.println("\t\t\t\t\tJohny won with " + Johny.getXp()
                + "XP.");
    }
}
