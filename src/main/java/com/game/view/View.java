package com.game.view;

import com.game.controller.Controller;

/**
 * Class for start program.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public final class View {

    /**
     * @see View#View()
     */
    private View() {
    }

    /**
     * @param args arguments
     * @see View#main(String[])
     */
    public static void main(final String[] args) {
        TextInfo.welcoming();
        TextInfo.beginning();
        Controller.start();
    }
}
