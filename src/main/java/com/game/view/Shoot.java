package com.game.view;

import static com.game.helper.Constants.*;

/**
 * Class for drawing duel
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public final class Shoot {

    /**
     * @see Shoot#Shoot()
     */
    private Shoot() {
    }

    /**
     * @see Shoot#leftShoot()
     */
    private static void leftShoot() {
        System.out.print("0-");
        for (int i = 0; i < SHOOT_DRAWING_LENGTH + SHOOT_DRAWING_LENGTH; i++) {
            System.out.print(". ");
            try {
                Thread.sleep(TIME);
            } catch (java.lang.InterruptedException e) {
            }
        }
        try {
            Thread.sleep(BREAK_BEFORE_DELETING);
        } catch (java.lang.InterruptedException e) {
        }
        for (int i = 0; i < SHOOT_DRAWING_LENGTH + SHOOT_DRAWING_LENGTH; i++) {
            System.out.print("\b\b");
        }
    }

    /**
     * @see Shoot#rightShoot()
     */
    private static void rightShoot() {
        for (int i = 0; i < SHOOT_DRAWING_LENGTH + SHOOT_DRAWING_LENGTH; i++) {
            System.out.print(". ");
        }
        try {
            Thread.sleep(BREAK_BEFORE_DELETING);
        } catch (java.lang.InterruptedException e) {
        }
        for (int i = 0; i < SHOOT_DRAWING_LENGTH + SHOOT_DRAWING_LENGTH; i++) {
            System.out.print("\b\b");
            try {
                Thread.sleep(TIME);
            } catch (java.lang.InterruptedException e) {
            }
        }
        System.out.println(SPACE_BEFORE_PICTURE+"-0");
        System.out.println("Press \"Space+Enter\".");
    }

    /**
     * @see Shoot#leftShootDesign()
     */
    static void leftShootDesign() {
        System.out.print("0-");
        for (int i = 0; i < SHOOT_DRAWING_LENGTH; i++) {
            System.out.print(". ");
            try {
                Thread.sleep(TIME);
            } catch (java.lang.InterruptedException e) {
            }
        }
    }

    /**
     * @see Shoot#rightShootDesign()
     */
    static void rightShootDesign() {
        for (int i = 0; i < SHOOT_DRAWING_LENGTH; i++) {
            System.out.print(". ");
            try {
                Thread.sleep(TIME);
            } catch (java.lang.InterruptedException e) {
            }
        }
        System.out.print("-0");
    }

    /**
     * @see Shoot#savedJohny()
     */
    public static void savedJohny() {
        Shoot.leftShoot();
        System.out.println(SPACE_BEFORE_PICTURE+"(\\0");
        System.out.println("Press \"0+Enter\".");
    }

    /**
     * @see Shoot#killedJohny()
     */
    public static void killedJohny() {
        Shoot.leftShoot();
        System.out.println(SPACE_BEFORE_PICTURE+"\\0/");
        System.out.println("Press \"0+Enter\".");
    }

    /**
     * @see Shoot#savedPlayer()
     */
    public static void savedPlayer() {
        System.out.print("0/)");
        Shoot.rightShoot();
    }

    /**
     * @see Shoot#killedPlayer()
     */
    public static void killedPlayer() {
        System.out.print("\\0/");
        Shoot.rightShoot();
    }
}
