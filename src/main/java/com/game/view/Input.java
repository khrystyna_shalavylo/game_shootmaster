package com.game.view;

import java.util.Scanner;

/**
 * Class for input information.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public final class Input {
    /**
     * @value in - Scanner object
     */
    private static Scanner in = new Scanner(System.in);

    /**
     * @see Input#Input()
     */
    private Input() {
    }

    /**
     * @see Input#input(String)
     */
    public static void input(String button) {
        boolean isRightButton = false;
        do {
            if (in.nextLine().matches(button)) {
                isRightButton = true;
            }
        } while (isRightButton);
    }
}
