package com.game.controller;

import com.game.model.Harm;
import com.game.model.Johny;
import com.game.model.Player;
import com.game.view.Input;
import com.game.view.Shoot;
import com.game.view.TextInfo;

import static com.game.helper.Constants.*;

/**
 * Class for combining view amd model.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class Controller {
    /**
     * @value johny - Johny object
     */
    private static Johny johny = new Johny();
    /**
     * @value player - Player object
     */
    private static Player player = new Player();

    private static String current;

    /**
     * @see Controller#Controller()
     */
    public Controller() {
    }

    /**
     * @return String Access to current
     * @see Controller#getCurrent()
     */
    private static String getCurrent() {
        return current;
    }

    /**
     * @param shooter - setting current shooter
     * @see Controller#setCurrent(String)
     */
    private static void setCurrent(final String shooter) {
        current = shooter;
    }

    /**
     * @return String shooter who will begin
     * @see Controller#shooterSelection()
     */
    public final String shooterSelection() {
        int randValue = (int) (Math.random() * MAX_SECOND);
        if (randValue == 1) {
            current = player.getName();
        } else {
            current = johny.getName();
        }
        return current;
    }

    /**
     * @return double getting shoot duration
     * @see Controller#shooting()
     */
    public static double shooting() {
        long start = System.currentTimeMillis();
        Input.input(" \n");
        return (double) (System.currentTimeMillis() - start);
    }

    /**
     * @return double getting save duration
     * @see Controller#saving()
     */
    public static double saving() {
        long start = System.currentTimeMillis();
        Input.input("0\n");
        return  (double) (System.currentTimeMillis() - start);
    }

    /**
     * @see Controller#start()
     */
    public static void start() {
        boolean isEndGame;
        do {
            for (int i = 0; i < SHOOTS_IN_ITERATION; i++) {
                if (Johny.getXp() > 0 && Player.getXp() > 0) {
                    Controller.doHarm();
                } else {
                    Controller.result();
                    return;
                }
            }
            System.out.println(player.getName() + ": " + Player.getXp()
                    + "XP\t\t\t\t\t\t\t\t\t\t\t" + johny.getName() + ": "
                    + Johny.getXp() + "XP");
            try {
                Thread.sleep(BREAK_BETWEEN_ITERATIONS);
            } catch (java.lang.InterruptedException e) {
            }
            isEndGame = Controller.ifEndGame();
        } while (isEndGame);
    }

    /**
     * @see Controller#result()
     */
    private static void result() {
        if (Johny.getXp() <= 0) {
            TextInfo.winning();
        } else {
            TextInfo.losing();
        }
    }

    /**
     * @see Controller#doHarm()
     */
    private static void doHarm() {
        if (Controller.getCurrent().equals(player.getName())) {
            if (Harm.doHarmJohny()) {
                Shoot.savedJohny();
            } else {
                Shoot.killedJohny();
            }
            Controller.setCurrent(johny.getName());
        } else {
            if (Harm.doHarmPlayer()) {
                Shoot.savedPlayer();
            } else {
                Shoot.killedPlayer();
            }
            Controller.setCurrent(player.getName());
        }
    }

    /**
     * @return boolean returns flag
     * @see Controller#ifEndGame()
     */
    private static boolean ifEndGame() {
        boolean isEndGame;
        if (Johny.getXp() <= 0) {
            if (Player.getXp() <= 0) {
                isEndGame = true;
            } else {
                isEndGame = false;
            }
        } else {
            if (Player.getXp() <= 0) {
                isEndGame = false;
            } else {
                isEndGame = true;
            }
        }
        return isEndGame;
    }
}
