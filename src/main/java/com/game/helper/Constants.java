package com.game.helper;

public class Constants {
    /**
     * @value SHOOTS_IN_ITERATION - count of shoots before intermediate result
     */
    public static final int SHOOTS_IN_ITERATION = 6;
    /**
     * @value MAX_SECOND - max time for saving or shooting
     */
    public static final int MAX_SECOND = 2;
    /**
     * @value BREAK_BETWEEN_ITERATIONS - break before continuing work
     */
    public static final int BREAK_BETWEEN_ITERATIONS = 3000;
    /**
     * @value POINTS_KILL - subtraction xp when killed
     */
    public static final int POINTS_KILL = 20;
    /**
     * @value POINTS_SAVE - adding xp when saved
     */
    public static final int POINTS_SAVE = 5;
    /**
     * @value SHOOTS_IN_ITERATION - time for shoot, break between shoots drawing
     */
    public static final int TIME = 200;
    /**
     * @value SHOOT_DRAWING_LENGTH - length of shoot points
     */
    public static final int SHOOT_DRAWING_LENGTH = 12;
    /**
     * @value BREAK_BEFORE_DELETING - break before deleting
     */
    public static final int BREAK_BEFORE_DELETING = 300;
    /**
     * @value SPACE_BEFORE_PICTURE - space before opponent's picture
     */
    public static final String SPACE_BEFORE_PICTURE = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
}
