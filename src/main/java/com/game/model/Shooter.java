package com.game.model;

/**
 * Interface.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public interface Shooter {
    /**
     * @value XP max shooter's xp
     */
    int XP = 100;

    /**
     * @return String name of shooterg
     * @see Shooter#getName()
     */
    String getName();
}
