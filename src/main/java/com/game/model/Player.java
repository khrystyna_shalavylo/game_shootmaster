package com.game.model;

/**
 * Class for describe Player shooter.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class Player implements Shooter {

    /**
     * @value xp - Player's xp
     */
    private static int xp;

    /**
     * @see Player#Player()
     */
    public Player() {
        xp = Shooter.XP;
    }

    /**
     * @return String getting Player's name
     * @see Player#getName()
     */
    public final String getName() {
        return "You";
    }

    /**
     * @return int getting Player's name
     * @see Player#getXp()
     */
    public static int getXp() {
        return xp;
    }

    /**
     * @see Player#setXp(int)
     * @param pxp setting Player's xp
     */
    static void setXp(final int pxp) {
        xp = pxp;
    }
}
