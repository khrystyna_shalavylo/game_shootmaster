package com.game.model;

/**
 * Class for describe Johny shooter.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public class Johny implements Shooter {

    /**
     * @value xp - Johny's xp
     */
    private static int xp;

    /**
     * @see Johny#Johny()
     */
    public Johny() {
        xp = Shooter.XP;
    }

    /**
     * @return String getting Johny's name
     * @see Johny#getName()
     */
    public final String getName() {
        return "Johny";
    }

    /**
     * @return int getting Johny's xp
     * @see Johny#getXp() )
     */
    public static int getXp() {
        return xp;
    }

    /**
     * @param pxp setting Johny's xp
     * @see Johny#setXp(int)
     */
    static void setXp(final int pxp) {
        xp = pxp;
    }
}
