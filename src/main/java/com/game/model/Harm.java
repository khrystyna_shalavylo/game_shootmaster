package com.game.model;

import com.game.controller.Controller;

import static com.game.helper.Constants.*;

/**
 * Class for harming shooters.
 *
 * @author Khrystyna Shalavylo
 * @version 1.0
 */
public final class Harm {

    /**
     * @value millisecond - time for shoot or save
     */
    private static double millisecond = 2000;

    /**
     * @see Harm#Harm()
     */
    private Harm() {
    }

    /**
     * @return boolean returns info if you harmed Johny
     * @see Harm#doHarmJohny()
     */
    public static boolean doHarmJohny() {
        boolean isSaved;
        if (Controller.shooting() < millisecond) {
            Johny.setXp(Johny.getXp() - POINTS_KILL);
            isSaved = false;
        } else {
            Johny.setXp(Johny.getXp() + POINTS_SAVE);
            isSaved = true;
        }
        if (millisecond > 2 * TIME) {
            millisecond = millisecond - TIME;
        }
        return isSaved;
    }

    /**
     * @return boolean returns info if you harmed Player
     * @see Harm#doHarmPlayer()
     */
    public static boolean doHarmPlayer() {
        boolean isSaved;
        if (Controller.saving() < millisecond) {
            Player.setXp(Player.getXp() + POINTS_SAVE);
            isSaved = true;
        } else {
            Player.setXp(Player.getXp() - POINTS_KILL);
            isSaved = false;
        }
        if (millisecond > 2 * TIME) {
            millisecond = millisecond - TIME;
        }
        return isSaved;
    }
}
